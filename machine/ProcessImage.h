/**********************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the CoffeeMachine example.
 *
 * The CoffeeMachine example is free software: you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public License as
 *published by the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 * The CoffeeMachine example is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CoffeeMachine example. If not, see
 *<http://www.gnu.org/licenses/>.
 *
 ***********************************************************************************/

#pragma once

#include <chrono>
#include <pdservpp.h>

constexpr std::chrono::milliseconds task_cycle_time {10};

struct ProcessImage
{
    struct Mill
    {
        Mill(PdServ::Task task);

        bool N01_mill_active = false;
    } mill_;

    struct WaterSupply
    {
        WaterSupply(PdServ::Task task);
        bool L10_level_not_low_ = false;

    } water_supply_;

    struct Boiler
    {
        float L11_level       = 0;
        int P12_pressure      = 0;
        bool Y04_inlet_valve_ = false;
        bool N02_inlet_pump_  = false;
        bool heating_         = false;

        bool filled() const { return L11_level >= 95; }
        bool too_low() const { return L11_level <= 10; }

        Boiler(PdServ::Task task);
    } boiler_;

    struct BrewingUnit
    {
        bool G20_hatch_closed_         = false;
        bool Y07_steam_inlet_valve_    = false;
        bool Y08_coffee_inlet_valve_   = false;
        bool Y09_coffee_grounds_valve_ = false;

        BrewingUnit(PdServ::Task task);
    } brewing_unit_;

    struct GroundsTray
    {
        bool G21_present_ = false;

        GroundsTray(PdServ::Task task);
    } grounds_tray_;

    struct MilkFrothingUnit
    {
        bool N03_milk_pump_  = false;
        int Y05_steam_inlet_ = 0;

        MilkFrothingUnit(PdServ::Task task);
    } milk_frothing_unit_;

    struct CocoaSupply
    {
        bool N14_cocoa_pump_ = false;

        CocoaSupply(PdServ::Task task);
    } cocoa_supply_;

    struct UI
    {
        char inputs_ = 0;

        UI(PdServ::Task task);
    } ui_;

    ProcessImage(PdServ::Task task);
};
