/**********************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the CoffeeMachine example.
 *
 * The CoffeeMachine example is free software: you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public License as
 *published by the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 * The CoffeeMachine example is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CoffeeMachine example. If not, see
 *<http://www.gnu.org/licenses/>.
 *
 ***********************************************************************************/

#include "ProcessImage.h"

ProcessImage::ProcessImage(PdServ::Task task) :
    mill_(task),
    water_supply_(task),
    boiler_(task),
    brewing_unit_(task),
    grounds_tray_(task),
    milk_frothing_unit_(task),
    cocoa_supply_(task),
    ui_(task)
{}

ProcessImage::Mill::Mill(PdServ::Task task)
{
    task.signal(1, "/coffee/mill/active", N01_mill_active);
}

ProcessImage::WaterSupply::WaterSupply(PdServ::Task task)
{
    task.signal(1, "/water/supply/level_low", L10_level_not_low_);
}

ProcessImage::Boiler::Boiler(PdServ::Task task)
{
    task.signal(1, "/water/boiler/fill_level/actual", L11_level);
    task.signal(1, "/water/boiler/pressure/actual", P12_pressure);
    task.signal(1, "/water/boiler/heating", heating_);
}

ProcessImage::BrewingUnit::BrewingUnit(PdServ::Task task)
{
    task.signal(1, "/brewing_unit/hatch_closed", G20_hatch_closed_);
}
ProcessImage::GroundsTray::GroundsTray(PdServ::Task task)
{
    task.signal(1, "/grounds_tray/present", G21_present_);
}
ProcessImage::MilkFrothingUnit::MilkFrothingUnit(PdServ::Task)
{}
ProcessImage::CocoaSupply::CocoaSupply(PdServ::Task)
{}

ProcessImage::UI::UI(PdServ::Task task)
{
    task.signal(1, "/ui/inputs", inputs_);
}
