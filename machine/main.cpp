/**********************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the CoffeeMachine example.
 *
 * The CoffeeMachine example is free software: you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public License as
 *published by the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 * The CoffeeMachine example is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CoffeeMachine example. If not, see
 *<http://www.gnu.org/licenses/>.
 *
 ***********************************************************************************/

#include "Controller.h"
#include "ProcessImage.h"
#include "VirtualIO.h"

#include <atomic>
#include <mutex>
#include <signal.h>
#include <time.h>

// RAII helper to configure the shell

void increment(timespec &ts)
{
    ts.tv_nsec += std::chrono::nanoseconds(task_cycle_time).count();
    if (ts.tv_nsec >= std::nano::den) {
        ++ts.tv_sec;
        ts.tv_nsec -= std::nano::den;
    }
}

std::atomic_bool running {true};

void signal_handler(int)
{
    running = false;
}

constexpr clockid_t used_clock = CLOCK_MONOTONIC;

int get_clock(timespec *ts)
{
    return clock_gettime(used_clock, ts);
}

int main()
{
    signal(SIGINT, signal_handler);
    std::mutex mut;
    PdServ::Server server("CoffeeMachine", "1.0", get_clock);
    server.set_parameter_writelock(mut);
    auto task = server.create_task(task_cycle_time, "Main Task");
    task.set_signal_readlock(mut);

    ProcessImage pi(task);
    VirtualIO io(pi);
    Controller control(pi, server);
    timespec ts;

    server.prepare();
    get_clock(&ts);
    while (running) {
        increment(ts);
        switch (clock_nanosleep(used_clock, TIMER_ABSTIME, &ts, nullptr)) {
            case 0:
                break;
            case -EINTR:
                return 0;
            default:
                return -1;
        }

        {
            std::unique_lock<std::mutex> lck(mut);
            io.update();
            control.update(ts);
        }
        task.update(ts);
    }
    return 0;
}
