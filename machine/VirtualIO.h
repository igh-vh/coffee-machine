/**********************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the CoffeeMachine example.
 *
 * The CoffeeMachine example is free software: you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public License as
 *published by the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 * The CoffeeMachine example is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CoffeeMachine example. If not, see
 *<http://www.gnu.org/licenses/>.
 *
 ***********************************************************************************/

#pragma once

#include <atomic>
#include <pthread.h>


struct ProcessImage;
class VirtualIO;

class ConsoleInput
{
  public:
    ConsoleInput(const VirtualIO &io);
    ConsoleInput(ConsoleInput const &)            = delete;
    ConsoleInput &operator=(ConsoleInput const &) = delete;
    ~ConsoleInput();
    mutable std::atomic_char value_ {0};

  private:
    const VirtualIO &io_;
    pthread_t reader_thread_;
    static void *run(void *self_v);
};


class VirtualIO
{
    ProcessImage &pi_;
    ConsoleInput const ci_    = {*this};
    float water_supply_level_ = 250;
    struct BrewingUnit
    {
        int coffee_ = 0;
    } brewing_unit_;
    struct Cup
    {
        float coffee_;
        int cocoa_;
        int milk_;
        float steam_;
    } cup_          = {};
    int tray_level_ = 0;

    friend ConsoleInput;

  public:
    VirtualIO(ProcessImage &pi);

    void update();
};
