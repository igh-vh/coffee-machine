/**********************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the CoffeeMachine example.
 *
 * The CoffeeMachine example is free software: you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public License as
 *published by the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 * The CoffeeMachine example is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CoffeeMachine example. If not, see
 *<http://www.gnu.org/licenses/>.
 *
 ***********************************************************************************/

#include "Controller.h"

#include "ProcessImage.h"

template <class Impl>
void SubController<Impl>::update(ProcessImage &pi)
{
    if (actual_runtime_ <= 0) {
        // currently stopped
        if (stored_start_ == start_) {
            // no edge, so nothing to do.
            return;
        }
        // edge detected, try to start
        stored_start_ = start_;
        status_       = static_cast<Impl &>(*this).start(pi);
        if (status_ != Error) {
            actual_runtime_ = desired_runtime_;
        }
        return;
    }
    // already running

    actual_runtime_ -= task_cycle_time.count();
    if (actual_runtime_ <= 0) {
        // runtime has expired
        static_cast<Impl &>(*this).stop(pi);
        status_ = Idle;
    }
    else if (!static_cast<Impl &>(*this).good(pi)) {
        static_cast<Impl &>(*this).stop(pi);
        status_         = Error;
        actual_runtime_ = 0;
    }
}

template <class Impl>
SubController<Impl>::SubController(
        const std::string &prefix,
        PdServ::ServBase &server,
        PdServ::Task task)
{
    server.parameter(
            (prefix + "/runtime/desired").c_str(), 0666, desired_runtime_);

    server.parameter((prefix + "/start").c_str(), 0666, start_);
    task.signal(1, (prefix + "/runtime/actual").c_str(), actual_runtime_);
    task.signal(1, (prefix + "/status").c_str(), status_);
}

Controller::Controller(ProcessImage &pi, PdServ::ServBase &server) :
    controller_task_(server.create_task(task_cycle_time, "Controller Task")),
    mill_(server, controller_task_),
    boiler_(server, controller_task_),
    brewing_unit_(server, controller_task_),
    milk_froth_(server, controller_task_),
    cocoa_(server, controller_task_),
    pi_(pi)
{}

void Controller::update(const timespec &ts)
{
    mill_.update(pi_);
    boiler_.update(pi_);
    brewing_unit_.update(pi_, ts);
    milk_froth_.update(pi_);
    cocoa_.update(pi_);

    controller_task_.update(ts);
}

bool Controller::Mill::good(const ProcessImage &pi) const
{
    return !pi.brewing_unit_.Y07_steam_inlet_valve_
            and !pi.brewing_unit_.Y09_coffee_grounds_valve_;
}

UnitStatus Controller::Mill::start(ProcessImage &pi)
{
    if (!good(pi))
        return Error;
    pi.brewing_unit_.Y08_coffee_inlet_valve_ = true;
    pi.mill_.N01_mill_active                 = true;
    return Operation1;
}

void Controller::Mill::stop(ProcessImage &pi)
{
    pi.brewing_unit_.Y08_coffee_inlet_valve_ = false;
    pi.mill_.N01_mill_active                 = false;
}


void Controller::Boiler::update(ProcessImage &pi)
{  // boiler level
    if (pi.water_supply_.L10_level_not_low_ and !pi.boiler_.filled()
        and refill_active_ and pi.boiler_.L11_level <= desired_level_) {
        pi.boiler_.N02_inlet_pump_  = true;
        pi.boiler_.Y04_inlet_valve_ = true;
    }
    else {
        pi.boiler_.N02_inlet_pump_  = false;
        pi.boiler_.Y04_inlet_valve_ = false;
    }

    // boiler heat
    pi.boiler_.heating_ = !pi.boiler_.too_low()
            and pi.boiler_.P12_pressure < desired_pressure_;
}

bool Controller::BrewingUnit::good(
        const ProcessImage &pi,
        UnitStatus current_status) const
{
    if (current_status == Operation1) {
        return !clean_ and pi.brewing_unit_.G20_hatch_closed_
                and !pi.brewing_unit_.Y08_coffee_inlet_valve_
                and !pi.brewing_unit_.Y09_coffee_grounds_valve_;
    }
    else if (current_status == Operation2) {
        return clean_ and !pi.brewing_unit_.Y07_steam_inlet_valve_
                and !pi.brewing_unit_.Y08_coffee_inlet_valve_
                and pi.grounds_tray_.G21_present_;
    }
    else {
        return false;
    }
}

UnitStatus Controller::BrewingUnit::start(ProcessImage &pi)
{
    if (good(pi, Operation1)) {
        pi.brewing_unit_.Y07_steam_inlet_valve_ = true;
        return Operation1;
    }
    else if (good(pi, Operation2)) {
        pi.brewing_unit_.Y09_coffee_grounds_valve_ = true;
        return Operation2;
    }
    else {
        return Error;
    }
}

void Controller::BrewingUnit::stop(ProcessImage &pi)
{
    pi.brewing_unit_.Y09_coffee_grounds_valve_ = false;
    pi.brewing_unit_.Y07_steam_inlet_valve_    = false;
}

void Controller::BrewingUnit::update(ProcessImage &pi, const timespec &ts)
{
    SubController::update(pi);
    // hatch
    if (!pi.brewing_unit_.G20_hatch_closed_ and !hatch_event_was_set_) {
        door_open_.set(WARN_EVENT, ts);
        hatch_event_was_set_ = true;
    }
    else if (pi.brewing_unit_.G20_hatch_closed_ and hatch_event_was_set_) {
        door_open_.reset(ts);
        hatch_event_was_set_ = false;
    }
}


UnitStatus Controller::MilkFroth::start(ProcessImage &pi)
{
    if (!good(pi))
        return Error;
    pi.milk_frothing_unit_.Y05_steam_inlet_ = steam_quantity_ * 100;
    pi.milk_frothing_unit_.N03_milk_pump_   = true;
    return Operation1;
}
void Controller::MilkFroth::stop(ProcessImage &pi)
{
    pi.milk_frothing_unit_.N03_milk_pump_   = false;
    pi.milk_frothing_unit_.Y05_steam_inlet_ = 0;
}
bool Controller::MilkFroth::good(const ProcessImage &pi) const
{
    return pi.boiler_.P12_pressure >= MinimumPressure;
}


UnitStatus Controller::Cocoa::start(ProcessImage &pi)
{
    pi.cocoa_supply_.N14_cocoa_pump_ = true;
    return Operation1;
}
void Controller::Cocoa::stop(ProcessImage &pi)
{
    pi.cocoa_supply_.N14_cocoa_pump_ = false;
}

Controller::Mill::Mill(PdServ::ServBase &server, PdServ::Task task) :
    SubController("/coffee/mill", server, task)
{}

Controller::Boiler::Boiler(PdServ::ServBase &server, PdServ::Task task)
{
    task.signal(1, "/water/boiler/status", status_);
    server.parameter("/water/boiler/fill_level/desired", 0666, desired_level_);
    server.parameter("/water/boiler/pressure/desired", 0666, desired_pressure_);
    server.parameter("/water/boiler/refill_active", 0666, refill_active_);
}
Controller::BrewingUnit::BrewingUnit(
        PdServ::ServBase &server,
        PdServ::Task task) :
    SubController("/brewing_unit", server, task),
    door_open_(server, "/brewing_unit/hatch_open", "Brewing unit hatch is open")
{
    server.parameter("/brewing_unit/clean", 0666, clean_);
}
Controller::MilkFroth::MilkFroth(PdServ::ServBase &server, PdServ::Task task) :
    SubController("/milk/froth", server, task)
{
    server.parameter("/milk/froth/steam_quantity", 0666, steam_quantity_);
}

Controller::Cocoa::Cocoa(PdServ::ServBase &server, PdServ::Task task) :
    SubController("/cocoa", server, task)
{}
