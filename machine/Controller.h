/**********************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the CoffeeMachine example.
 *
 * The CoffeeMachine example is free software: you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public License as
 *published by the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 * The CoffeeMachine example is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CoffeeMachine example. If not, see
 *<http://www.gnu.org/licenses/>.
 *
 ***********************************************************************************/

#pragma once

#include <pdservpp.h>

struct ProcessImage;

enum UnitStatus {
    Idle       = 0,
    Operation1 = 1,
    Operation2 = 2,
    Error      = -1,
};

template <class Impl>
class SubController
{
    int desired_runtime_ = 0;
    unsigned char start_ = 0, stored_start_ = 0;
    int status_ = Idle;

  protected:
    int actual_runtime_ = 0;

  public:
    UnitStatus status() const { return static_cast<UnitStatus>(status_); }

    SubController(
            const std::string &prefix,
            PdServ::ServBase &server,
            PdServ::Task task);

    void update(ProcessImage &pi);
};

class Controller
{
    PdServ::Task controller_task_;

    struct Mill : SubController<Mill>
    {
        Mill(PdServ::ServBase &server, PdServ::Task task);

        UnitStatus start(ProcessImage &pi);
        void stop(ProcessImage &pi);
        bool good(const ProcessImage &pi) const;
    } mill_;


    struct Boiler
    {
        int status_           = Idle;
        int desired_level_    = 0;
        int desired_pressure_ = 0;
        bool refill_active_   = 0;

        Boiler(PdServ::ServBase &server, PdServ::Task task);
        void update(ProcessImage &pi);
    } boiler_;

    struct BrewingUnit : SubController<BrewingUnit>
    {
        bool clean_        = false;
        int _clean_counter = 0;
        PdServ::Event door_open_;
        bool hatch_event_was_set_ = false;

        BrewingUnit(PdServ::ServBase &server, PdServ::Task task);

        UnitStatus start(ProcessImage &pi);
        void stop(ProcessImage &pi);
        bool good(const ProcessImage &pi) const { return good(pi, status()); }
        bool good(const ProcessImage &pi, UnitStatus current_status) const;
        void update(ProcessImage &pi, const timespec &ts);
    } brewing_unit_;

    struct MilkFroth : SubController<MilkFroth>
    {
        static constexpr int MinimumPressure = 1200;

        float steam_quantity_ = 0;

        MilkFroth(PdServ::ServBase &server, PdServ::Task task);

        UnitStatus start(ProcessImage &pi);
        void stop(ProcessImage &pi);
        bool good(const ProcessImage &pi) const;
    } milk_froth_;

    struct Cocoa : SubController<Cocoa>
    {
        Cocoa(PdServ::ServBase &server, PdServ::Task task);

        UnitStatus start(ProcessImage &pi);
        void stop(ProcessImage &pi);
        bool good(const ProcessImage &) const { return true; }
    } cocoa_;

    ProcessImage &pi_;

  public:
    Controller(ProcessImage &pi, PdServ::ServBase &server);
    void update(const timespec &ts);
};
