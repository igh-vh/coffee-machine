/**********************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the CoffeeMachine example.
 *
 * The CoffeeMachine example is free software: you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public License as
 *published by the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 * The CoffeeMachine example is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CoffeeMachine example. If not, see
 *<http://www.gnu.org/licenses/>.
 *
 ***********************************************************************************/

#include "VirtualIO.h"

#include "ProcessImage.h"

#include <atomic>
#include <iostream>
#include <pthread.h>
#include <stdexcept>
#include <sys/select.h>
#include <termios.h>
#include <unistd.h>


extern std::atomic_bool running;

ConsoleInput::ConsoleInput(const VirtualIO &io) : io_(io)
{
    if (pthread_create(&reader_thread_, nullptr, ConsoleInput::run, this) != 0)
        throw std::runtime_error("could not create thread");
}

ConsoleInput::~ConsoleInput()
{
    pthread_cancel(reader_thread_);
    pthread_join(reader_thread_, nullptr);
}

// RAII helper to configure the shell
class TerminalGuard
{
    termios old_;

  public:
    TerminalGuard()
    {
        tcgetattr(STDIN_FILENO, &old_);
        termios newt = old_;
        newt.c_lflag &= ~(ECHO | ICANON);
        tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    }
    ~TerminalGuard() { tcsetattr(STDIN_FILENO, TCSANOW, &old_); }
};


void *ConsoleInput::run(void *self_v)
{
    const TerminalGuard tg;
    auto &This = *reinterpret_cast<ConsoleInput *>(self_v);
    fd_set fds;

    for (char c = 0; c != 'q';) {
        printf("Cup: (K|C|M|S) = (%.01f|%d|%d|%d), Water supply: %.02f       "
               "\r",
               This.io_.cup_.coffee_, This.io_.cup_.cocoa_, This.io_.cup_.milk_,
               (int) This.io_.cup_.steam_, This.io_.water_supply_level_);

        FD_ZERO(&fds);
        FD_SET(STDIN_FILENO, &fds);
        timeval timeout {0, 200000};
        const auto s =
                select(STDIN_FILENO + 1, &fds, nullptr, nullptr, &timeout);
        if (s == 1) {
            std::cin >> c;
            This.value_ = c;
        }
        else if (s != 0) {
            break;
        }
    }
    running = false;
    return nullptr;
}


void VirtualIO::update()
{
    pi_.ui_.inputs_ = 0;
    switch (ci_.value_) {
        case 'w':
            pi_.water_supply_.L10_level_not_low_ = true;
            water_supply_level_                  = 250;
            break;
        case 'o':
            pi_.grounds_tray_.G21_present_ = false;
            break;
        case 's':
            pi_.grounds_tray_.G21_present_ = true;
            tray_level_                    = 0;
            break;
        case 'a':
            pi_.brewing_unit_.G20_hatch_closed_ = false;
            break;
        case 'z':
            pi_.brewing_unit_.G20_hatch_closed_ = true;
            break;
        case 't':
            cup_ = {};
            break;

        default:
            pi_.ui_.inputs_ = ci_.value_;
    };

    // heating
    if (pi_.boiler_.heating_ and pi_.water_supply_.L10_level_not_low_)
        pi_.boiler_.P12_pressure += 1;

    // water supply
    if (pi_.boiler_.N02_inlet_pump_ and pi_.boiler_.Y04_inlet_valve_
        and water_supply_level_ > 0) {
        water_supply_level_ -= .05;
        pi_.boiler_.L11_level += .05;
    }

    // mill
    if (pi_.mill_.N01_mill_active
        and pi_.brewing_unit_.Y08_coffee_inlet_valve_) {
        brewing_unit_.coffee_++;
    }

    // coffee grounds
    if (pi_.brewing_unit_.Y09_coffee_grounds_valve_
        and brewing_unit_.coffee_ > 0) {
        --brewing_unit_.coffee_;
        ++tray_level_;
    }

    // cocoa
    if (pi_.cocoa_supply_.N14_cocoa_pump_) {
        cup_.cocoa_ += 2;
    }

    // milk
    if (pi_.milk_frothing_unit_.N03_milk_pump_) {
        cup_.milk_++;
    }
    if (pi_.milk_frothing_unit_.Y05_steam_inlet_ > 0
        and pi_.milk_frothing_unit_.Y05_steam_inlet_ < 1) {
        cup_.steam_ += pi_.milk_frothing_unit_.Y05_steam_inlet_
                * pi_.boiler_.P12_pressure / 10;
    }

    // coffee
    if (pi_.brewing_unit_.Y07_steam_inlet_valve_ and pi_.boiler_.P12_pressure
        and pi_.boiler_.L11_level > 0 and pi_.boiler_.P12_pressure > 500) {
        cup_.coffee_ += brewing_unit_.coffee_ / 4;
        pi_.boiler_.L11_level -= 0.01;
    }
}

VirtualIO::VirtualIO(ProcessImage &pi) : pi_(pi)
{
    pi_.brewing_unit_.G20_hatch_closed_  = true;
    pi_.water_supply_.L10_level_not_low_ = true;
}
