# Coffee Machine

This is a demo project which shows how to build a sequencer with the Python asyncio PdCom5 interface.

## P&I Diagram

![Piping and Instrumentation Diagram](CoffeeMachine.svg)

## CLI of the machine

| Key | Action                                  |
|---  | ---                                     |
| q   | Quit                                    |
| c   | Make a cappuccino                       |
| e   | Make an espresso                        |
| k   | Make a hot chocolate                    |
| t   | Take cup                                |
| w   | Refill water                            |
| o   | Open caffe grounds tray                 |
| s   | Close caffee grounds tray               |
| a   | Open brewing unit hatch                 |
| z   | Close brewing unit hatch                |


## PdServ signals and parameters

| Type     | Path                              |  Description                         | PCPI Point |
|---       |---                                |---                                   |---         |
| P int    | /coffee/mill/runtime/desired      | Desired Mill Runtime [ms]            |            |
| S int    | /coffee/mill/runtime/actual       | Actual Mill Runtime left [ms]        |            |
| S uint   | /coffee/mill/status               | Mill status                          |            |
| P uchar  | /coffee/mill/start                | Mill start trigger                   |            |
| S bool   | /water/supply/level_low           | Water supply tank is low             | L10        |
| P int    | /water/boiler/fill_level/desired  | Boiler given fill level              |            |
| S int    | /water/boiler/fill_level/actual   | Boiler actual fill level             | L11        |
| P bool   | /water/boiler/refill_active       | Machine is allowed to refill boiler  |            |
| P int    | /water/boiler/pressure/desired    | Desired boiler pressure [mbar]       |            |
| S int    | /water/boiler/pressure/actual     | Actual boiler pressure [mbar]        | P12        |
| S bool   | /water/boiler/heating             | Boiler heating is on                 |            |
| S uint   | /water/boiler/status              | Boiler status                        |            |
| S bool   | /brewing_unit/hatch_closed        | Brewing unit hatch is closed         | G20        |
| P int    | /brewing_unit/runtime/desired     | Desired brew duration [ms]           |            |
| S int    | /brewing_unit/runtime/actual      | Brew duration left [ms]              |            |
| P bool   | /brewing_unit/clean               | Remove coffee grounds                |            |
| S uint   | /brewing_unit/status              | Brewing unit status                  |            |
| P uchar  | /brewing_unit/start               | Brewing unit start trigger           |            |
| P float  | /milk/froth/steam_quantity        | Amount of steam in milk froth [0..1] |            |
| P int    | /milk/froth/runtime/desired       | Desired milk flow runtime [ms]       |            |
| S int    | /milk/froth/runtime/actual        | Milk flow runtime left [ms]          |            |
| S uint   | /milk/froth/status                | Milk Froth unit status               |            |
| P uchar  | /milk/froth/start                 | Milk Froth unit start trigger        |            |
| P int    | /cocoa/runtime/desired            | Desired Cocoa runtime [ms]           |            |
| S int    | /cocoa/runtime/actual             | Cocoa runtime left [ms]              |            |
| S uint   | /cocoa/status                     | Cocoa unit status                    |            |
| P uchar  | /cocoa/start                      | Cocoa unit start trigger             |            |
| S bool   | /grounds_tray/present             | Grounds tray is installed correctly  | G21        |
| S char   | /ui/inputs                        | User Interface Buttons               |            |
