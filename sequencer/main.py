# Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
#
# This file is part of the CoffeeMachine example.
#
# The CoffeeMachine example is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# The CoffeeMachine example is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the CoffeeMachine example.
# If not, see <http://www.gnu.org/licenses/>.

from asyncio import CancelledError, get_event_loop, Task, get_event_loop_policy
from enum import IntEnum
from pdcom5 import (
    Process,
    TemporaryValueChange,
    Transmission,
    Variable,
    Watchdog,
)
from signal import SIGINT, SIGTERM


class UnitStatus(IntEnum):
    Error = -1
    Idle = 0
    Operation1 = 1
    Operation2 = 2


class MachineUnit:
    async def wait_for_error(self):
        await self._status.waitUntilValueEquals(UnitStatus.Error)

    def good(self):
        return self._status.value != UnitStatus.Error


class RunnableMachineUnit(MachineUnit):
    async def run_for(self, time: float):
        await self._runtime.setValue((int)(time * 1000))
        await self._start.variable.setValue(self._start.value + 2)
        await self._status.waitUntilValueEquals(UnitStatus.Idle)


async def createMachineUnit(process: Process, path: str):
    runtime = await process.find(path + "/runtime/desired")
    start = await process.find(path + "/start")
    if runtime is None or start is None:
        ans = MachineUnit()
    else:
        ans = RunnableMachineUnit()
        ans._start = await process.subscribe(Transmission.event_mode, start)
        await ans._start.poll()
        ans._runtime = runtime
    mode = Transmission.event_mode
    ans._status = await process.subscribe(mode, path + "/status")
    return ans


machine_units = {
    "mill": "/coffee/mill",
    "boiler": "/water/boiler",
    "brewing_unit": "/brewing_unit",
    "milk_froth": "/milk/froth",
    "cocoa": "/cocoa",
}


class Sequencer:
    def __init__(self, process):
        super().__init__()
        self._process: Process = process
        self.current_recipe: Task = None
        self._machine_units: "dict[str, MachineUnit]" = {}
        self._steam_quantity: Variable = None
        self._refill_active: Variable = None

    @classmethod
    async def create(self, process: Process):
        ans = self(process)
        ans._refill_active = await process.find("/water/boiler/refill_active")
        ans._steam_quantity = await process.find("/milk/froth/steam_quantity")
        for name, path in machine_units.items():
            unit = await createMachineUnit(process, path)
            ans._machine_units[name] = unit
        return ans

    def getWatchTasks(self):
        return self._machine_units.values()

    async def bark(self):
        print("Bark!")
        if self.current_recipe is not None:
            task = self.current_recipe
            self.current_recipe = None
            task.cancel()
            await task

    async def espresso_recipe(self):
        await self._machine_units["mill"].run_for(10.5)
        async with self._refill_active.temporaryFalse():
            await self._machine_units["brewing_unit"].run_for(4)
        print("Brewing done")

    async def cappuccino_recipe(self):
        await self._machine_units["mill"].run_for(10.5)
        async with TemporaryValueChange(self._steam_quantity, 0.4, 0.0):
            await self._machine_units["milk_froth"].run_for(3.2)
        await self._machine_units["brewing_unit"].run_for(6.5)

    async def cocoa_recipe(self):
        await self._machine_units["cocoa"].run_for(4.5)
        async with TemporaryValueChange(self._steam_quantity, 0.3, 0.0):
            await self._machine_units["milk_froth"].run_for(6.1)

    def start_recipe(self, recipe):
        if self.current_recipe is not None:
            if not self.current_recipe.done():
                raise RuntimeError("Busy")
            self.current_recipe.result()
        self.current_recipe = get_event_loop().create_task(recipe(self))


async def startup(p: Process, wait_seconds):
    # fill up boiler and heat up
    given_level = 80
    given_pressure = 1800
    await p.setVariableValue("/water/boiler/pressure/desired", given_pressure)
    await p.setVariableValue("/water/boiler/fill_level/desired", given_level)
    await p.setVariableValue("/water/boiler/refill_active", True)
    sub = p.create_subscriber(0.5)
    await sub.waitUntilAllValuesGreaterEqual(
        {
            "/water/boiler/fill_level/actual": given_level,
            "/water/boiler/pressure/actual": given_pressure,
        },
        wait_seconds,
    )


async def main():
    p = Process()
    await p.connect("msr://localhost")
    print("Connected!")
    await startup(p, 20)
    print("startup done")
    input = await p.subscribe(Transmission.event_mode, "/ui/inputs")
    sequencer = await Sequencer.create(p)
    async with Watchdog(sequencer.bark, sequencer.getWatchTasks()):
        try:
            while True:
                (cmd, _) = await input.read()
                print("cmd", cmd)
                if cmd == ord(b"c"):
                    sequencer.start_recipe(Sequencer.cappuccino_recipe)
                elif cmd == ord(b"e"):
                    sequencer.start_recipe(Sequencer.espresso_recipe)
                elif cmd == ord(b"k"):
                    sequencer.start_recipe(Sequencer.cocoa_recipe)
        finally:
            if sequencer.current_recipe is not None:
                await sequencer.current_recipe


if __name__ == "__main__":
    loop = get_event_loop_policy().get_event_loop()
    main_task = loop.create_task(main())
    for signal in [SIGINT, SIGTERM]:
        loop.add_signal_handler(signal, main_task.cancel)
    try:
        loop.run_until_complete(main_task)
    except CancelledError:
        pass
    finally:
        loop.close()
